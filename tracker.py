import time
import requests
import random as rd
import bencodepy
import struct
import time
from socket import *
class tracker_info():
    def __init__(self, peer):
         self.compact = 1 
         self.request_parameters = {
              'info_hash' : peer.info_hash,
              'peer_id'   : peer.peer_ID,
              'port'      : peer.port,
              'uploaded'  : peer.num_uploaded,
              'downloaded': peer.num_downloaded,
              'left'      : 0,
              'compact'   : self.compact,
              'event'     : 'started'
         }
         self.interval   = None
         self.complete   = None
         self.incomplete = None
         self.peers_list = []
         self.transaction_id = int(rd.randrange(0, 255))
         self.connection_id = 0x41727101980
         self.action = 0x0
    def HTTP_tracker_request(self,tracker_url):
         try:
             raw_response = requests.get(tracker_url,self.request_parameters,timeout=10)
             bencoded_dict = bencodepy.decode(raw_response.content)
             self.HTTP_tracker_response_parser(bencoded_dict)
             self.HTTP_tracker_getpeers(bencoded_dict)
             return self.peers_list
         except Exception as e:
             #print(e)
             return self.peers_list
    def HTTP_tracker_response_parser(self,bencoded_dict):
         if b'complete' in bencoded_dict:
             self.complete = bencoded_dict[b'complete']
         if b'incomplete' in bencoded_dict:
             self.complete = bencoded_dict[b'incomplete']
         #if b'interval' in bencoded_dict[b'interval']:
             #self.interval = bencoded_dict[b'interval']
    def HTTP_tracker_getpeers(self,bencoded_dict):
         if b'peers' in bencoded_dict:
             raw_list = bencoded_dict[b'peers']
             encoded_list = []
             for i in range(0,len(raw_list),6):
                  encoded_list.append(raw_list[i:i+6])
             for i in encoded_list:
                  ip = ''
                  for j in range(0,4):
                      ip = ip + str(int(i[j])) + '.'
                  ip = ip[0:len(ip)-1]
                  port = i[4]*256 + i[5]
                  self.peers_list.append([ip,port])
    def UDP_tracker_request(self,tracker_url):
         udp_url = tracker_url[6:len(tracker_url):].split(':')
         url = udp_url[0]
         port = int(udp_url[1].split('/')[0])
         udp_socket = socket(AF_INET, SOCK_DGRAM)
         request_data = self.UDP_conn_data()
         try:
            # get the connection id from the connection request 
            prev = self.connection_id
            self.UDP_conn_request(udp_socket,request_data,url,port)
            if prev == self.connection_id:
                udp_socket.close()
                return []
            #print("connection_id from tracker - ",self.connection_id)
            self.action = 0x1
            announce_data = ''
            announce_data =  struct.pack("!q", self.connection_id)    
            announce_data += struct.pack("!i", 2)  
            announce_data += struct.pack("!i", 123456)
            hash = self.request_parameters['info_hash']
            #print("hash=",hash)
            announce_data += struct.pack("!20s", hash)
            announce_data += struct.pack("!20s", self.request_parameters['peer_id'])         
            announce_data += struct.pack("!q", self.request_parameters['downloaded'])
            announce_data += struct.pack("!q", self.request_parameters['left']) 
            announce_data += struct.pack("!q", self.request_parameters['uploaded']) 
            announce_data += struct.pack("!i", 2) 
            announce_data += struct.pack("!i", 0) 
            announce_data += struct.pack("!i", int(rd.randrange(0, 255)))
            announce_data += struct.pack("!i", -1) 
            announce_data += struct.pack("!H", self.request_parameters['port'])
            self.peers_list = self.UDP_announce_request(announce_data,udp_socket,url,port)
            return self.peers_list
         except Exception as error_msg:
            # close the socket if the response is not obtained
            print("no response")
            udp_socket.close()
            return self.peers_list
    def UDP_conn_data(self):    #it builds the request data
        request_data = ''
        request_data  = struct.pack("!q", self.connection_id)
        request_data += struct.pack("!i", self.action)    
        request_data += struct.pack("!i", self.transaction_id)
        return request_data   #16 bytes total(8+4+4)
    def UDP_conn_request(self,udp_socket,request_data,url,port):
        udp_socket.sendto(request_data,(url,port))
        udp_socket.settimeout(8)
        try:
            raw_data = udp_socket.recvfrom(4096)[0]
            #print(raw_data)
            res_action = struct.unpack_from("!i", raw_data)[0]
            res_transaction_id = struct.unpack_from("!i", raw_data, 4)[0]
            self.connection_id = struct.unpack('!q',raw_data[8:16])[0]
            return
        except Exception as e:
            print("error:",e)
            return
    def UDP_announce_request(self,announce_data,udp_socket,url,port):
        announce_response = None
        trails = 0
        flag = 1
        self.peers_list = []
        while flag == 1:
            # try connection request after some interval of time
            try:
                udp_socket.sendto(announce_data, (url,port))
                #print("announce request send")
                raw_announce_response, conn = udp_socket.recvfrom(4096)
                #print("res=",len(raw_announce_response))
                flag = 0
                #print(raw_announce_response)
                response_action = struct.unpack_from("!i",raw_announce_response)[0]
                response_transaction_id = struct.unpack_from("!i",raw_announce_response, 4)[0]
                offset = 8
                self.interval = struct.unpack_from("!i",raw_announce_response, offset)[0]
                offset = offset + 4
                self.leechers = struct.unpack_from("!i",raw_announce_response, offset)[0]
                offset = offset + 4
                self.seeders = struct.unpack_from("!i",raw_announce_response, offset)[0]
                offset = offset + 4
                while(offset != len(raw_announce_response)):
                    raw_peer_data = raw_announce_response[offset : offset + 6]    

                    
                    peer_IP = ".".join(str(int(a)) for a in raw_peer_data[0:4])
                    
                    peer_port = raw_peer_data[4] * 256 + raw_peer_data[5]
               
                    
                    self.peers_list.append([peer_IP, peer_port])
                    offset = offset + 6
                return self.peers_list
            except Exception as e:
                print(e)
        return self.peers_list
def create_tracker_info(p):
    return tracker_info(p)

