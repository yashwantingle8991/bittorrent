import sys
import torrent_reader_new as torrent_read
from socket import *
import tracker
import random as rd
import struct
import math
from threading import Thread
import time
class peer():
    def __init__(self, peer_IP, peer_port, torrent_file):
        self.ip = peer_IP
        self.port = peer_port
        self.torrent  = torrent_read.extract_torrent(torrent_file)
        self.peer_ID = '01234567890123456789'.encode()     #here is .encode()           
        self.connection_ID = None           #initially none then get by tracker
        self.active_peers_connected = []    #initialize list of connected peers
        self.tracker_list = self.torrent.tracker_list
        self.info_hash = self.torrent.info_hash
        self.uploaded = set([])
        self.downloaded = set([])
        self.num_uploaded = 0
        self.num_downloaded = 0
        self.num_left = self.torrent.piece_length
        self.no_of_pieces = self.torrent.no_of_pieces
        self.no_of_bytes = int(math.ceil(int(self.no_of_pieces) / 8.0))
        self.have = [0]*self.no_of_pieces
        self.piece_length = self.torrent.piece_length
        self.file_name = self.torrent.file_name
class peer_protocol():
    def __init__(self, peer):
        self.peer_id = peer.peer_ID
        self.info_hash = peer.info_hash
        self.protocol = "BitTorrent protocol"
        self.handshake_flag = 0
        self.choked = 1
        self.state = 0
        self.no_of_bytes = peer.no_of_bytes
        self.no_of_pieces = peer.no_of_pieces
        self.connected_peers = []
        self.bitfield = []
        for i in range(self.no_of_bytes):
             self.bitfield.append(0)
        self.piece_data = [' ']*self.no_of_pieces
    def handshake_info(self):
        handshake_data  = struct.pack("!B", len(self.protocol))
        handshake_data += struct.pack("!19s", self.protocol.encode())
        handshake_data += struct.pack("!Q", 0x0)
        handshake_data += struct.pack("!20s", self.info_hash)
        handshake_data += struct.pack("!20s", self.peer_id)
        return handshake_data
    def do_handshake(self,peer_list):
        response = None
        for i in peer_list:
             tcp_socket = socket(AF_INET,SOCK_STREAM)
             if i not in self.connected_peers:
                 flag = 0
                 while True:
                   try:
                      tcp_socket.connect((i[0],i[1]))
                      handshake_data = self.handshake_info()
                      tcp_socket.send(handshake_data)
                      flag = 1
                      break
                   except Exception as e:
                      print(e)
                      break
                   #time.sleep(10)
                   #break
                 if flag == 1:
                    try:
                      response = tcp_socket.recv(2048)
                      if self.response_check(response):
                         l = []
                         l.append(tcp_socket)
                         l.append(response[48:68].decode())
                         l.append([i[0],i[1]])
                         return l
                    except Exception as e:
                      print(e)
        return []
    def response_check(self,response):
        if response == None:
             return False
        if len(response) != 68:
             return False
        if response[28:48] != self.info_hash:
             return False
        if response[48:68] == self.peer_id:
             return False
        return True
    def send_interested(self,tcp_socket):
        m  = struct.pack("!I", 1)
        m += struct.pack("!B", 2)
        tcp_socket.send(m)
    def send_unchoke(self,tcp_socket):
        m  = struct.pack("!I", 1)
        m += struct.pack("!B", 1)
        tcp_socket.send(m)
    def send_choke(self,tcp_socket):
        m  = struct.pack("!I", 1)
        m += struct.pack("!B", 0)
        tcp_socket.send(m)
    def send_request(self,tcp_socket,index,begin):
        m = struct.pack("!IB"+"III", 13, 6, index, begin, self.no_of_bytes)
        tcp_socket.send(m)
    def check_reponse(self,response):
        id = 0
        for i in response:
            if i != 0:
               id = i
               break
        if id == 0:
           return 0
        elif id == 1:
           return 1
        elif id == 5:
           return 5
        elif id == 7:
           return 7
    def download_peer(self,response):
        if response == []:
            return
        tcp_socket = response[0]
        conn_peer_id = response[1]
        peer_info = response[2]
        print("connected to peer -")
        self.connected_peers.append(peer_info)
        print("id :",conn_peer_id)
        print("ip :",peer_info[0],"port:",peer_info[1])
        peer_proto.send_interested(tcp_socket)
        peer_proto.handshake_flag = 1
        while peer_proto.handshake_flag == 1:
             data = tcp_socket.recv(2048)
             res = struct.unpack("B"*len(data), data)
             #print(res)
             if list(res) == []:
                 return
             message_id = peer_proto.check_reponse(list(res))
             if message_id == -1:
                 peer_proto.choked = 1
                 print("choked by :",conn_peer_id)
                 tcp_socket.close()
                 self.connected_peers.remove(peer_info)
                 return
             elif message_id == 1:
                 #print(res)
                 print("unchoked by :",conn_peer_id)
                 peer_proto.choked = 0
                 break
             elif message_id == 5:
                 print("recieved bitfield by :",conn_peer_id)
                 print("length=",len(res))
             elif message_id == 4:
                 print("recieved have message by :",conn_peer_id)
        i = 0
        while iscomplete(p) == 0 and peer_proto.choked == 0:
             i = piece_index(p)
             if i == -1:
                 break
             j = 0
             peice = ''
             while j < p.piece_length//2**14:
                 peer_proto.send_request(tcp_socket,i,j)
                 print("piece resuest sent to ", conn_peer_id,"for index",i,"block no",j,"========>")
                 data = tcp_socket.recv(2048)
                 if j == 0:
                    block = data[8:]
                 else:
                    block = data
                 peice = peice + str(block) 
                 if res == []:
                    break
                 print("<=========receiving block from :",peer_info)
                 j=j+1
             print("recieved piece")
             self.piece_data[i] = peice
             p.have[i] = 1
        return
def iscomplete(peer):
     for i in range(0,peer.no_of_pieces):
         if peer.have[i] == 0:
            return 0
     return 1
def piece_index(peer):
     if iscomplete(peer) == 0:
         while True:
            i = rd.randint(0,peer.no_of_pieces-1)
            if peer.have[i] == 0:
               return i
     return -1
def write_file(peice_data):
   try:
      f = open("/home/dell/Downloads/"+"p.file_name","r")
   except Exception as e:
      print("Can't open")
      return
   for i in range(0,len(peice_data)):
       f.write(peice_data[i])
   return
def create_peer():
    f = str(sys.argv[1])
    p = peer('127.0.0.1',6881,f)
    return p
p = create_peer()
peer_proto = peer_protocol(p)      #here is the line added to create an object
tracker_data = tracker.create_tracker_info(p)
flag = 1
while flag:                        #here is the while loop added (for the scenario in case it fails to get peers list)
    for i in p.tracker_list:
        if i[0] == 'u':
           p.active_peers_connected = tracker_data.UDP_tracker_request(i)
        if i[0] == 'h':
           p.active_peers_connected = tracker_data.HTTP_tracker_request(i)
        if p.active_peers_connected != []:
           flag = 0
           break
response = []
while iscomplete(p) == 0:
      response = peer_proto.do_handshake(p.active_peers_connected)
      t = Thread(target=peer_proto.download_peer,args=(response,))
      t.start()
write_file(peer_proto.piece_data)
