import sys
import bencodepy
import hashlib
#storing data from .torrent file 
class torrent_data():
    def __init__(self, tracker_list, file_name, file_size, piece_length, pieces, info_hash, files):
        self.tracker_list  = tracker_list     
        self.file_name      = file_name             
        self.file_size      = file_size                 
        self.piece_length   = piece_length             
        self.pieces         = pieces                    
        self.info_hash      = info_hash                 
        self.files          = files             #for multitorrent file only    
def extract_torrent(f):
     with open(f,'rb') as f:
       meta_data = f.read()
     data = bencodepy.decode(meta_data)
     tracker_list = []
     info_list = data[b'info']
     encoding = 'UTF-8'
     files = None
     if b'encoding' in data:
        ncoding = data[b'encoding'].decode('utf-8')
     if b'announce-list' in data:
       for i in data[b'announce-list']:
          l = [x.decode('utf-8') for x in i]
          tracker_list.append(l[0])
     elif b'announce' in data:
       tracker_list.append(data[b'announce'].decode())
     file_name = info_list[b'name'].decode(encoding)
     if b'files' in info_list:
          files = info_list[b'files']
          file_size = files[0][b'length']
     else:
          file_size = info_list[b'length']
     piece_length = info_list[b'piece length']
     pieces = info_list[b'pieces']
     sha1_hash = hashlib.sha1()
     sha1_hash.update(bencodepy.encode(info_list))
     info_hash = sha1_hash.digest()
     return torrent_data(tracker_list, file_name, file_size, piece_length, pieces, info_hash, files)
t = extract_torrent(str(sys.argv[1]))
